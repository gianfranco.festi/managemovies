package com.managemovies.dao;
import com.managemovies.domain.Nazionalita;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface NazionalitaDao extends
    PagingAndSortingRepository<Nazionalita,String>, JpaRepository<Nazionalita,String>{


}
