package com.managemovies.domain;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Entity
@Table(name = "attori")
public class Attore{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cod_attore")
    private Long codAttore;

    private String nome;
    @Column(name = "anno_nascita")
    private int annoNascita;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nazionalita")
    @JsonIdentityReference
    private Nazionalita nazionalita;
    
    @ManyToMany(fetch = FetchType.LAZY,cascade={CascadeType.MERGE,CascadeType.PERSIST})
        @JoinTable(name = "recita",
        inverseJoinColumns = @JoinColumn(name = "cod_film"),
        joinColumns= @JoinColumn(name = "cod_attore")
    )
    @JsonIgnore
    private Set<Film> films; 
}