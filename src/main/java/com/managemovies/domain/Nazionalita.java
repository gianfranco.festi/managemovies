package com.managemovies.domain;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Entity
@Table(name="nazionalita")
public class Nazionalita{
    @Id
    @Column(name="nazionalita")
    private String nazionalita;
      
    @OneToMany(
        mappedBy = "nazionalita",
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    @JsonIgnore
    Set<Attore> attori;
    @OneToMany(
        mappedBy = "nazionalita",
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    @JsonIgnore
    Set<Film> films;
}