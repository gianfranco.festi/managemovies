package com.managemovies.controllers;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

import com.managemovies.dao.AttoriDao;
import com.managemovies.dao.FilmDao;
import com.managemovies.domain.Attore;
import com.managemovies.domain.Film;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
@CrossOrigin()
@RestController
@RequestMapping(value = "/api/attori", produces = "application/json")
public class AttoriController{
    @Autowired
    private AttoriDao attoriDao;
    @Autowired
    private FilmDao filmDao;
    
    @GetMapping(value="")
    public List<Attore> getAttori(
            @RequestParam(value="page", required=false) Integer page,
            @RequestParam(value="size", required=false) Integer size){
        if (page == null)  page=0; 
        if (size == null)  size=Integer.MAX_VALUE;      
                System.out.println(String.format("page %s, size %s",page,size));
        return attoriDao.findAll(new PageRequest(page, size)).getContent();
    }
    
    @GetMapping(value="recita/id/{codAttore}")
    public Set<Film> getRecita(@PathVariable Long codAttore){
        return attoriDao.getOne(codAttore).getFilms();
        //findAllRecitaByCodAttore(codAttore);
    }
    @PostMapping("/save")
    public Attore save(@RequestBody Attore attore){
        return attoriDao.save(attore);
    }
    @PutMapping("/update")
    public Attore update(@RequestBody Attore attore){
        return attoriDao.saveAndFlush(attore);
    }
    
    @PostMapping("/addRecita/{codAttore}/{codFilm}")
    public ResponseEntity addfilm(@PathVariable Long codAttore,@PathVariable Long codFilm){
        return  this.filmDao.findById(codFilm).map((film) -> { 
            return this.attoriDao.findById(codAttore).map (attore -> {
                film.getAttori().add(attore);
                this.filmDao.save(film);
                return  ResponseEntity.ok().build();
            })
            .orElseGet(() ->  ResponseEntity.notFound().build());
            //ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.APPLICATION_JSON).body("{\"codAttore\":" + codAttore + "}");  
        })
        .orElseGet(() ->  ResponseEntity.notFound().build());
        //ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.APPLICATION_JSON).body("{\"codFilm\":" + codFilm + "}"));
    }

    @DeleteMapping("/delRecita/{codAttore}/{codFilm}")
    public ResponseEntity delRecita(@PathVariable Long codAttore,@PathVariable Long codFilm){
        return  this.filmDao.findById(codFilm).map((film) -> { 
            return this.attoriDao.findById(codAttore).map(attore -> {
                if (film.getAttori().remove(attore)){
                    if (attore.getFilms().remove(film)){
                        filmDao.flush();
                        attoriDao.flush();
                        return  ResponseEntity.ok().build(); //ok cancellazione recita
                    }
                }
                return ResponseEntity.notFound().build(); // codFilm + codAttore non recita 
            })
            .orElseGet(() ->  ResponseEntity.notFound().build()); //attore non esiste
        })    
        .orElseGet(() ->  ResponseEntity.notFound().build());//film non esiste
    }

    @GetMapping("/id/{id}")
    public Attore getAttore(@PathVariable Long id){
        System.out.println(String.format("id = %s", id));
        return attoriDao.findById(id).get();
    }
    @DeleteMapping("/id/{id}")
    public ResponseEntity deleteAttore(@PathVariable long id){
        return attoriDao.findById(id)
            .map(attore -> {
                attoriDao.deleteById(id);
                return ResponseEntity.ok().build();
            })
            .orElseGet(() -> {  
                return  ResponseEntity.notFound().build(); 
            });
    }

    @GetMapping(value="NonRecita/id/{codAttore}")
    public Set<Film> getNonRecita(@PathVariable Long codAttore){
        return attoriDao.findAllNonrecitaByCodAttore(codAttore);
        
    }
}

//!