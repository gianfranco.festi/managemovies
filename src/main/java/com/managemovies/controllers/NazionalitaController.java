package com.managemovies.controllers;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;



import com.managemovies.dao.AttoriDao;
import com.managemovies.dao.FilmDao;
import com.managemovies.dao.NazionalitaDao;
import com.managemovies.domain.Attore;
import com.managemovies.domain.Film;
import com.managemovies.domain.Nazionalita;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping(value = "/api/nazionalita", produces = "application/json")
public class NazionalitaController{

    @Autowired
    private NazionalitaDao nazionalitaDao;
    @Autowired
    private AttoriDao attoriDao;
    @Autowired
    private FilmDao filmDao;

    @GetMapping(value="")
    public List<Nazionalita> getNazionalita() {
        return nazionalitaDao.findAll();
    }
    
    @PostMapping("/save")
    public Nazionalita save(@RequestBody Nazionalita nazionalita){
        System.out.println(nazionalita.getNazionalita());
        return nazionalitaDao.save(nazionalita);
    }

    @DeleteMapping("/{strnazionalita}")
    public ResponseEntity deleteNazionalita(@PathVariable String strnazionalita ){
        return this.nazionalitaDao.findById(strnazionalita).map(nazionalita ->
        {   
            // this.attoriDao.deleteByNazionalita(nazionalita);
            // this.filmDao.deleteByNazionalita(nazionalita);
            this.nazionalitaDao.delete(nazionalita);
 
            return ResponseEntity.ok().build();
        })
        .orElseGet(() -> {  
            return  ResponseEntity.notFound().build(); 
        });
    }
}