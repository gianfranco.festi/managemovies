package com.managemovies.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Entity
@Table(name="films")
public class Film {
    @Id
    @Column(name="cod_film")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    Long codFilm;

    @Column(name="titolo")
    String titolo;
    @Column(name="anno_produzione")
    int annoProduzione;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nazionalita")
    @JsonIdentityReference
    private Nazionalita nazionalita;
    
    @Column(name="regista")
    String regista;
    @Column(name="genere")
    String genere;

    @ManyToMany(fetch = FetchType.LAZY,cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    @JoinTable(name = "recita",
        joinColumns = @JoinColumn(name = "cod_film"), 
        inverseJoinColumns = @JoinColumn(name = "cod_attore")
    )
   // @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="codAttore")
    @JsonIdentityReference(alwaysAsId=true) 
    Set<Attore> attori;
}