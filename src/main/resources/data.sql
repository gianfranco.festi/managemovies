insert into nazionalita(nazionalita) values('Italiana'), ('USA'),('Giapponese'),('Francese');
INSERT INTO attori(cod_attore,nome,anno_nascita,nazionalita) VALUES 
    (1,'M.Mastroianni',1940,'Italiana'),
    (2,'S.Lorenn',1945,'Italiana'),
    (3,'A.Falchi',1970,'Italiana'),
    (4,'P.Anderson',1974,'USA'),
    (5,'K.Basinger',1972,'USA'),
    (6,'B.Bardot',2000,'Francese');

INSERT INTO films(cod_film,titolo,anno_produzione,nazionalita,regista,genere) VALUES 
    (1,'Lultima sequenza',2002,'Italiana','Rossi','Sentimentale'),
    (2,'Tre vite e una sola morte',2000,'Italiana','Rossi','Commedia'),
    (3,'La miliardaria',1960,'Italiana','Rossi','Commedia'),
    (4,'Cuori Estranei',1970,'Italiana','Bianchi','Sentimentale'),
    (5,'Lo spazio',1980,'Giapponese','Furgonzin','Fantascienza'),
    (6,'Giro del mondo',1991,'Giapponese','Furgonzin','Fantascienza'),
    (7,'Il quinto elemento',1997,'Francese','Besson','Fantascienza'),
    (8,'Marte',1988,'Francese','Baget','Fantascienza'),
    (9,'Venere',1999,'Francese','Clio','Azione'),
    (10,'Giove',2000,'Giapponese','Ciu','Azione'),
    (11,'Casablanca',1995,'Italiana','Rossi','Drammatico');

INSERT INTO recita(cod_attore,cod_film) VALUES (1,2),(1,3),(3,4),(3,5),(1,6),(2,8),(3,8),(4,8),(4,9),(5,10),(1,11);    