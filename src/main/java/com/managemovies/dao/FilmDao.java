package com.managemovies.dao;

import com.managemovies.domain.Film;
import com.managemovies.domain.Nazionalita;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@RestResource(path="films",rel="attori")
@Repository
public interface FilmDao extends JpaRepository<Film,Long>{
    @Modifying
    @Transactional
    @Query("DELETE Film f WHERE f.nazionalita = ?1")
    void deleteByNazionalita(Nazionalita nazionalita);

}