package com.managemovies.dao;
import java.util.List;
import java.util.Set;

import com.managemovies.domain.Attore;
import com.managemovies.domain.Film;
import com.managemovies.domain.Nazionalita;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.jboss.logging.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface AttoriDao extends
    PagingAndSortingRepository<Attore,Long>,JpaRepository<Attore,Long>{
    @Modifying
    @Transactional
    @Query("DELETE Attore a WHERE a.nazionalita = ?1")
    void deleteByNazionalita(Nazionalita nazionalita);

    @Query("SELECT f FROM Film f JOIN f.attori a WHERE  a.codAttore <>?1")
    Set<Film> findAllNonrecitaByCodAttore(Long codAttore);
  
}
