package com.managemovies.controllers;

import java.util.List;
import java.util.Set;

import com.managemovies.dao.FilmDao;
import com.managemovies.domain.Film;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@RequestMapping(path="/api/film", produces = "application/json")
public class FilmController{
    @Autowired
    private FilmDao filmDao;

    @GetMapping(value="")
    public List<Film> getFilms() {
        return this.filmDao.findAll();
    }
    
}